var express = require('express');
var router = express.Router();

router.get('/login', function(req, res){
    res.render('login');
});

router.get('/signup', function(req, res){
    res.render('signup');
});

router.get('/profile', function(req, res){
    res.render('profile');
});
router.get('/explore', function(req, res){
    res.render('explore');
});
module.exports = router;
